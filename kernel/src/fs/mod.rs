/// [Битмап](https://en.wikipedia.org/wiki/Free-space_bitmap)
/// для отслеживания какие именно блоки файловой системы заняты, а какие свободны.
mod block_bitmap;

/// [Блочный кеш](https://en.wikipedia.org/wiki/Page_cache)
/// для ускорения работы с диском за счёт кеширования блоков диска в памяти.
mod block_cache;

/// Запись [директории](https://en.wikipedia.org/wiki/Directory_(computing)) с [`Inode`],
/// который содержится в этой директории, и его именем.
mod directory_entry;

/// Интерфейс для работы с [PATA](https://en.wikipedia.org/wiki/Parallel_ATA)--дисками.
mod disk;

/// Интерфейс к файлам и директориям файловой системы.
mod file;

/// Интерфейс к файловой системе.
mod file_system;

/// Метаинформация об объекте с данными --- [inode](https://en.wikipedia.org/wiki/Inode).
mod inode;

/// Суперблок
/// ([superblock](https://en.wikipedia.org/wiki/Unix_File_System#Design))
/// файловой системы.
mod superblock;


use ku::memory::Page;

pub(crate) use block_cache::BlockCache;

pub use directory_entry::MAX_NAME_LEN;
pub use file::File;
pub use file_system::FileSystem;
pub use inode::Kind;

// Used in docs.
#[allow(unused)]
use {block_bitmap::BlockBitmap, inode::Inode, superblock::Superblock};


/// Размер блока данных файловой системы.
const BLOCK_SIZE: usize = Page::SIZE;

/// Номер блока, в котором хранится суперблок
/// ([superblock](https://en.wikipedia.org/wiki/Unix_File_System#Design))
/// файловой системы --- [`Superblock`].
const SUPERBLOCK_BLOCK: usize = 1;

/// Номер первого блока, хранящего
/// [битмап](https://en.wikipedia.org/wiki/Free-space_bitmap)
/// занятых блоков --- [`BlockBitmap`].
const FIRST_BITMAP_BLOCK: usize = SUPERBLOCK_BLOCK + 1;


#[doc(hidden)]
pub mod test_scaffolding {
    pub use super::{
        block_bitmap::test_scaffolding::*,
        block_cache::test_scaffolding::*,
        file::test_scaffolding::*,
        inode::test_scaffolding::*,
    };


    pub const BLOCK_SIZE: usize = super::BLOCK_SIZE;
}
