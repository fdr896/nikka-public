use core::{cmp, fmt, iter::Filter, mem, ops::Add};

use chrono::{DateTime, Utc};
use static_assertions::const_assert;

use ku::{
    error::{
        Error::{FileExists, FileNotFound, InvalidArgument, NoDisk, NotDirectory, NotFile},
        Result,
    },
    memory::{size::Size, Block, Virt},
    time,
};

use super::{
    block_bitmap::BlockBitmap,
    block_cache::BlockCache,
    directory_entry::DirectoryEntry,
    BLOCK_SIZE,
};

// Used in docs.
#[allow(unused)]
use ku::error::Error;


/// Тип объекта с данными --- [inode](https://en.wikipedia.org/wiki/Inode).
#[derive(Clone, Copy, Debug, Default, Eq, PartialEq)]
#[repr(usize)]
pub enum Kind {
    /// Свободный [inode](https://en.wikipedia.org/wiki/Inode).
    #[default]
    Unused = 0,

    /// Файл.
    File = 1,

    /// Директория.
    Directory = 2,
}


/// Метаинформация об объекте с данными --- [inode](https://en.wikipedia.org/wiki/Inode).
#[derive(Debug, Default)]
#[repr(C)]
pub(super) struct Inode {
    /// Тип объекта с данными --- свободный, файл или директория.
    kind: Kind,

    /// Время последней модификации [`Inode`].
    modify_time: DateTime<Utc>,

    /// Размер данных в байтах.
    size: usize,

    /// Лес, отвечающий за отображение блоков [`Inode`]
    /// в номера блоков файловой системы.
    root_blocks: Forest,
}


impl Inode {
    /// Тип объекта с данными --- свободный, файл или директория.
    pub(super) fn kind(&self) -> Kind {
        self.kind
    }


    /// Устанавливает тип объекта с данными --- свободный, файл или директория.
    ///
    /// # Panics
    ///
    /// Паникует, если `kind` соответствует занятому [`Inode`],
    /// но он уже занят.
    pub(super) fn set_kind(&mut self, kind: Kind) {
        assert!(self.kind == Kind::Unused || kind == Kind::Unused);
        self.kind = kind;
    }


    /// Удаляет [`Inode`].
    pub(super) fn remove(&mut self, block_bitmap: &mut BlockBitmap) -> Result<()> {
        self.set_size(0, block_bitmap)?;
        assert!(self.root_blocks.iter().all(|&block| block == NO_BLOCK));
        self.set_kind(Kind::Unused);

        Ok(())
    }


    /// Размер данных в байтах.
    pub(super) fn size(&self) -> usize {
        self.size
    }


    /// Устанавливает размер данных в байтах.
    /// Если файл расширяется, то новые блоки с данными содержат нули.
    /// При необходимости выделяет или освобождает блоки через `block_bitmap`.
    /// Обновляет время последней модификации [`Inode`].
    ///
    /// Если новый размер `size` равен нулю, должен освободить все косвенные блоки,
    /// используемые в [`Inode::root_blocks`].
    pub(super) fn set_size(&mut self, size: usize, block_bitmap: &mut BlockBitmap) -> Result<()> {
        // TODO: your code here.
        unimplemented!();
    }


    /// Возвращает максимальный размер в байтах, которые может иметь файл.
    pub(super) fn max_size() -> usize {
        let max_blocks = leaf_count();

        max_blocks * BLOCK_SIZE
    }


    /// Время последней модификации [`Inode`].
    pub(super) fn modify_time(&self) -> DateTime<Utc> {
        self.modify_time
    }


    /// Находит занятую запись с именем `name` в директории.
    /// Возвращает ошибку [`Error::FileNotFound`], если такой записи нет.
    pub(super) fn find(&mut self, name: &str) -> Result<&mut Inode> {
        self.find_entry(name, None).and_then(|entry| {
            if entry.inode().kind == Kind::Unused {
                Err(FileNotFound)
            } else {
                Ok(entry.inode_mut())
            }
        })
    }


    /// Вставляет в директорию запись с именем `name` и типом `kind`.
    /// Обновляет как время модификации выделенной записи, так и время модификации самой директории.
    ///
    /// Возвращает ошибки:
    ///   - [`Error::FileExists`] если запись с таким именем уже есть.
    ///   - [`Error::InvalidArgument`] если `kind` равен [`Kind::Unused`].
    pub(super) fn insert(
        &mut self,
        name: &str,
        kind: Kind,
        block_bitmap: &mut BlockBitmap,
    ) -> Result<&mut Inode> {
        // TODO: your code here.
        unimplemented!();
    }


    /// Возвращает итератор по занятым записям директории.
    pub(super) fn list(&mut self) -> Result<List> {
        Ok(List(
            self.iter()?.filter(|entry| entry.inode().kind() != Kind::Unused),
        ))
    }


    /// Читает из файла по смещению `offset` в буфер `buffer` столько байт,
    /// сколько остаётся до конца файла или до конца буфера.
    ///
    /// Возвращает количество прочитанных байт.
    /// Если `offset` равен размеру файла, возвращает `0` прочитанных байт.
    ///
    /// Возвращает ошибки:
    ///
    /// - [`Error::NotFile`] если [`Inode`] не является файлом.
    /// - [`Error::InvalidArgument`] если `offset` превышает размер файла.
    pub(super) fn read(&mut self, offset: usize, buffer: &mut [u8]) -> Result<usize> {
        // TODO: your code here.
        unimplemented!();
    }


    /// Записывает в файл по смещению `offset` байты из буфера `buffer`.
    /// При необходимости расширяет размер файла.
    ///
    /// Возвращает количество записанных байт.
    /// Если `offset` превышает размер файла, расширяет файл нулями до заданного `offset`.
    ///
    /// Возвращает ошибку [`Error::NotFile`] если [`Inode`] не является файлом.
    pub(super) fn write(
        &mut self,
        offset: usize,
        buffer: &[u8],
        block_bitmap: &mut BlockBitmap,
    ) -> Result<usize> {
        // TODO: your code here.
        unimplemented!();
    }


    /// Находит занятую запись с именем `name` в директории.
    /// Если такой записи нет, возвращает свободную запись --- `Kind::Unused`.
    /// Если и таких нет, и при этом `block_bitmap` является [`Some`],
    /// пробует расширить директорию и вернуть новую свободную запись.
    fn find_entry(
        &mut self,
        name: &str,
        block_bitmap: Option<&mut BlockBitmap>,
    ) -> Result<&mut DirectoryEntry> {
        // TODO: your code here.
        unimplemented!();
    }


    /// Возвращает итератор по всем записям директории, --- и занятым, и свободным.
    fn iter(&mut self) -> Result<Iter> {
        if self.kind == Kind::Directory {
            assert!(self.size % BLOCK_SIZE == 0);

            Ok(Iter {
                block: Block::default(),
                block_number: 0,
                entry: 0,
                inode: self,
            })
        } else {
            Err(NotDirectory)
        }
    }


    /// По номеру блока `inode_block_number` внутри данных [`Inode`]
    /// возвращает ссылку на запись из метаданных этого [`Inode`].
    /// Эта запись предназначена для номера блока на диске,
    /// хранящего указанный блок данных [`Inode`].
    /// С помощью этого метода происходит отображение смещения внутри данных файла
    /// в номер блока на диске, хранящего эти данные.
    /// Номер `inode_block_number` равен смещению внутри данных файла,
    /// делённому на размер блока [`BLOCK_SIZE`].
    ///
    /// Если при обходе леса отображения блоков `Inode::root_blocks`
    /// встречается не выделенный косвенный блок, пробует выделить его с помощью `block_bitmap`.
    /// Если при этом `block_bitmap` равен [`None`], возвращает ошибку [`Error::NoDisk`].
    fn block_entry(
        &mut self,
        inode_block_number: usize,
        block_bitmap: Option<&mut BlockBitmap>,
    ) -> Result<&mut usize> {
        // TODO: your code here.
        unimplemented!();
    }


    /// Возвращает блок в памяти блочного кеша,
    /// где хранится блок `inode_block_number` внутри данных [`Inode`].
    fn block(&mut self, inode_block_number: usize) -> Result<Block<Virt>> {
        assert!(inode_block_number < self.size.div_ceil(BLOCK_SIZE));
        BlockCache::block(*self.block_entry(inode_block_number, None)?)
    }
}


impl fmt::Display for Inode {
    fn fmt(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        write!(
            formatter,
            "{{ {:?}, size: {} B = {}, modify: {} }}",
            self.kind(),
            self.size(),
            Size::bytes(self.size()),
            self.modify_time(),
        )
    }
}


/// Итератор по всем записям директории, --- и занятым, и свободным.
pub(super) struct Iter<'a> {
    /// Блок в памяти блочного кеша, внутри которого находится текущая позиция итератора.
    block: Block<Virt>,

    /// Номер блока внутри данных [`Inode`]
    /// для текущей позиции итератора.
    block_number: usize,

    /// Номер записи директории внутри блока для текущей позиции итератора.
    entry: usize,

    /// [`Inode`] директории.
    inode: &'a mut Inode,
}


impl<'a> Iter<'a> {
    /// Расширяет директорию свободными записями на один блок.
    fn extend(&mut self, block_bitmap: &mut BlockBitmap) -> Result<()> {
        self.inode.set_size(self.inode.size() + BLOCK_SIZE, block_bitmap)
    }
}


impl<'a> Iterator for Iter<'a> {
    type Item = &'a mut DirectoryEntry;


    fn next(&mut self) -> Option<Self::Item> {
        // TODO: your code here.
        unimplemented!();
    }
}


/// Итератор по занятым записям директории.
pub(super) struct List<'a>(Filter<Iter<'a>, fn(&&mut DirectoryEntry) -> bool>);


impl<'a> Iterator for List<'a> {
    type Item = &'a mut DirectoryEntry;


    fn next(&mut self) -> Option<Self::Item> {
        self.0.next()
    }
}


/// Лес, отвечающий за отображение блоков [`Inode`]
/// в номера блоков файловой системы.
type Forest = [usize; MAX_HEIGHT];


/// Вспомогательная функция для обхода леса отображения блоков [`Inode`].
/// По заданному количеству `tree_count` деревьев в лесу и номеру блока с данными `leaf_number`
/// выдает кортеж с номером нужного дерева, количеством листьев в этом дереве и номером листа
/// в этом дереве, который соответствует листу `leaf_number` леса.
fn find_leaf(leaf_in_forest: usize) -> Result<LeafCoordinates> {
    // TODO: your code here.
    unimplemented!();
}


/// Полное количество листьев в лесу [`Forest`] ---
/// максимальное количество блоков в одном [`Inode`].
fn leaf_count() -> usize {
    (0..MAX_HEIGHT)
        .map(leaf_count_in_tree)
        .reduce(Add::add)
        .expect("the forest is empty")
}


/// Количество листьев в дереве высоты `tree_height`.
fn leaf_count_in_tree(tree_height: usize) -> usize {
    INDIRECT_BLOCK_ARITY.pow(tree_height.try_into().expect("the tree height is off the chart"))
}


/// Удаляет дерево или поддерево на которое указывает запись `node`
/// из леса отображения блоков [`Inode`].
/// Высота поддерева передаётся в `height`.
/// Освобождаемые косвенные блоки передаются в `block_bitmap`.
fn remove_tree(node: &mut usize, height: usize, block_bitmap: &mut BlockBitmap) -> Result<()> {
    // TODO: your code here.
    unimplemented!();
}


/// Обходит дерево или поддерево на которое указывает запись `node`
/// из леса отображения блоков [`Inode`] в поисках листа номер
/// `leaf_number` в этом поддереве.
/// Высота поддерева передаётся в `height`, а количество листьев в нём --- в `leaf_count`.
/// При необходимости выделяет косвенные блоки --- узлы дерева --- из `block_bitmap`.
#[allow(unused_mut)] // TODO: remove before flight.
fn traverse<'a>(
    forest: &'a mut Forest,
    leaf_coordinates: LeafCoordinates,
    mut block_bitmap: Option<&mut BlockBitmap>,
) -> Result<&'a mut usize> {
    // TODO: your code here.
    unimplemented!();
}


/// Возвращает срез потомков узла `node` в лесу отображения блоков [`Inode`].
/// При необходимости, то есть когда `*node` равен [`NO_BLOCK`],
/// выделяет косвенный блок для них из `block_bitmap` и заполняет его записями [`NO_BLOCK`].
fn next_level<'a>(
    node: &'a mut usize,
    block_bitmap: &mut Option<&mut BlockBitmap>,
) -> Result<&'a mut [usize]> {
    // TODO: your code here.
    unimplemented!();
}


/// Координаты листа в лесу.
struct LeafCoordinates {
    /// Номер леста в его дереве.
    leaf: usize,

    /// Номер дерева в лесу [`Forest`].
    tree: usize,
}


const_assert!(BLOCK_SIZE % mem::size_of::<usize>() == 0);
const_assert!(BLOCK_SIZE % mem::size_of::<Inode>() == 0);


/// Арность дерева отображения блоков.
const INDIRECT_BLOCK_ARITY: usize = BLOCK_SIZE / mem::size_of::<usize>();

/// Максимальная высота дерева отображения блоков.
const MAX_HEIGHT: usize = 4;

/// Зарезервированный номер блока, означающий что блок не выделен.
const NO_BLOCK: usize = 0;


#[doc(hidden)]
pub mod test_scaffolding {
    use ku::error::Result;

    use super::super::test_scaffolding::BlockBitmap;

    use super::Kind;


    pub struct Inode(pub(in super::super) super::Inode);


    impl Inode {
        pub fn new(kind: Kind) -> Self {
            let mut inode = super::Inode::default();
            inode.set_kind(kind);
            Self(inode)
        }


        pub fn block_entry(
            &mut self,
            inode_block_number: usize,
            block_bitmap: &mut BlockBitmap,
        ) -> Result<&mut usize> {
            self.0.block_entry(inode_block_number, Some(&mut block_bitmap.0))
        }
    }
}
