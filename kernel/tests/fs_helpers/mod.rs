use ku::memory::size::MiB;

use kernel::{
    fs::{
        test_scaffolding::{block_cache_init, BlockBitmap, Inode, BLOCK_SIZE},
        Kind,
    },
    log::debug,
};


pub(super) fn single_inode_fs(kind: Kind) -> (BlockBitmap, Inode) {
    debug!(block_count = BLOCK_COUNT);

    block_cache_init(FS_DISK, BLOCK_COUNT).unwrap();

    BlockBitmap::format(BLOCK_COUNT).unwrap();

    let block_bitmap = BlockBitmap::new(BLOCK_COUNT).unwrap();
    let inode = Inode::new(kind);

    (block_bitmap, inode)
}


pub(super) const BLOCK_COUNT: usize = FS_SIZE / BLOCK_SIZE;

const FS_DISK: usize = 1;
const FS_SIZE: usize = 32 * MiB;
