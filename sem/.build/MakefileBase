.PHONY: default base base_no_miri base_no_miri_no_clippy lint test ok all

default: all

base: lint test

base_miri_unsafe: lint_unsafe test test_miri

base_miri_unsafe_no_std: lint_unsafe_no_std test test_miri

base_no_miri: lint test

base_no_miri_no_parallel: lint test_no_parallel

base_no_miri_no_clippy: lint_no_clippy test

lint_unsafe:
	cargo clippy
	@if [ "$$(cargo clippy 2>&1 | grep -v Finished | grep -v Checking)" ]; \
		then exit 1; \
	fi

	cargo fmt -- --check

lint_unsafe_no_std:
	cargo clippy
	@if [ "$$(cargo clippy 2>&1 | grep -v Finished | grep -v Checking)" ]; \
		then exit 1; \
	fi

	cargo fmt -- --check

	if [ "$$(head -n 1 ./src/lib.rs)" != "#![no_std]" ]; then \
		echo "'#![no_std]' must be at the top of lib.rs"; \
		exit 1; \
	fi \

lint: lint_no_clippy
	cargo clippy
	@if [ "$$(cargo clippy 2>&1 | grep -v Finished | grep -v Checking)" ]; \
		then exit 1; \
	fi

lint_no_clippy:
	cargo fmt -- --check

test:
	cargo test
	cargo test --release

test_miri:
	# cargo +nightly miri test

test_no_parallel:
	cargo test -j 1

ok:
	@echo 'OK!'

submit:
	cargo run --manifest-path ../../tools/submit/Cargo.toml -- --task-path .
	@echo 'OK!'
